import backend.Printer;
import frontend.Parser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        File inputFile;
        if (args.length > 0) {
            inputFile = new File(args[0]);
        } else {
            return;
        }

        if (!inputFile.canRead()) {
            System.err.printf("Cannot read file [%s]", inputFile.getAbsolutePath());
            return;
        }

        Parser parser = null;
        try (FileReader fileReader = new FileReader(inputFile)) {
            parser = new Parser();
            parser.parse(fileReader);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }
}
