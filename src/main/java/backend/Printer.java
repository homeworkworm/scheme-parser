package backend;

import intermediate.CloseBracket;
import intermediate.Expression;

import java.util.Stack;

public class Printer {
    public static void print(Expression rootExpression) {
        Stack<Item> stack = new Stack<>();
        stack.push(new Item(0, rootExpression));
        Item workingItem;
        Item previousItem = null;

        while (!stack.empty()) {
            workingItem = stack.pop();

            String spaces;
            boolean previousIsQuotation = previousItem != null && previousItem.expression.expressions.size() > 0 && "quote".equals(previousItem.expression.expressions.get(0).getValue());
            if (previousIsQuotation) {
                spaces = "";
            } else {
                spaces = new String(new char[workingItem.level]).replace("\0", "  ");
            }

            Expression workingExpression = workingItem.expression;
            String outputText;

            if (workingExpression.getClass() == CloseBracket.class) {
                outputText = ")\n";
            } else if (workingExpression.getClass() == Expression.class) {
                if (workingExpression.isFlat()) {
                    outputText = workingExpression.toString() + "\n";
                } else {
                    boolean isQuotation = workingExpression.expressions.size() > 0 && "quote".equals(workingExpression.get(0).getValue());
                    if (isQuotation) {
                        outputText = "'";
                        stack.push(new Item(workingItem.level, workingExpression.expressions.get(1)));
                    } else {
                        if (workingExpression.expressions.size() == 0) {
                            outputText = "()\n";
                        } else {
                            outputText = "(\n";
                            stack.push(new Item(workingItem.level, new CloseBracket()));
                            for (int i = workingExpression.expressions.size() - 1; i >= 0; i--) {
                                stack.push(new Item(workingItem.level + 1, workingExpression.expressions.get(i)));
                            }
                        }
                    }
                }
            } else {
//                outputText = String.format("%s ; %s\n", workingExpression.toString(), workingExpression.getClass().getSimpleName());
                outputText = String.format("%s\n", workingExpression);
            }

            System.out.printf(spaces + outputText);

            previousItem = workingItem;
        }
    }

    protected static class Item {
        public int level = 0;
        public Expression expression;
        public boolean isRightBracket;

        Item(int level, Expression expression) {
            this.level = level;
            this.expression = expression;
            isRightBracket = (expression == null);
        }
    }
}