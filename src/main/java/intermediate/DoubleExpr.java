package intermediate;

public class DoubleExpr extends AtomExpression {
    double value;

    public DoubleExpr(double value) {
        this.value = value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
