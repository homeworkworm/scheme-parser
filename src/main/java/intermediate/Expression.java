package intermediate;

import java.util.ArrayList;

public class Expression {
    public ArrayList<Expression> expressions = new ArrayList<>();
    public Expression parent;
    boolean isFlat = true;

    public boolean isFlat() {
        return isFlat;
    }

    public Expression() {
    }

    public Expression(Expression parent) {
        this.parent = parent;
    }

    public Object getValue() {
        return null;
    }

    @Override
    public String toString() {
        if (expressions.size() == 0) {
            return "()";
        }

//        if (isFlat) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("(");

            for (int i = 0; i < expressions.size(); i++) {
                stringBuilder.append(expressions.get(i).toString());

                if (i != expressions.size() - 1) {
                    stringBuilder.append(" ");
                }
            }

            stringBuilder.append(")");
            return stringBuilder.toString();
//        }

//        return "( ... )";
    }

    public boolean add(Expression expression) {
        // todo: determine if flat (could be shown in one line).
        expression.parent = this;

        if (isFlat && !(expression instanceof AtomExpression)) {
            isFlat = false;
        }

        return expressions.add(expression);
    }

//    public Expression removeLast() {
//        if (expressions.size() == 0) {
//            return null;
//        }
//
//        return expressions.remove(expressions.size() - 1);
//    }

    public Expression get(int index) {
        return expressions.get(index);
    }
}
