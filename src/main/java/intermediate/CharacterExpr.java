package intermediate;

public class CharacterExpr extends AtomExpression {
    char value;

    public CharacterExpr(char value) {
        this.value = value;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
