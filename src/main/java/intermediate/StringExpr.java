package intermediate;

public class StringExpr extends AtomExpression {
    String value;

    public StringExpr(String value) {
        this.value = value;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
