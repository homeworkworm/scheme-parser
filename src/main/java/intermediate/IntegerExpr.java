package intermediate;

public class IntegerExpr extends AtomExpression {
    int value;

    public IntegerExpr(int value) {
        this.value = value;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
