package intermediate;

public class ObjectExpr extends AtomExpression {
    String name;

    public ObjectExpr(String name) {
        this.name = name;
    }

    @Override
    public Object getValue() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
