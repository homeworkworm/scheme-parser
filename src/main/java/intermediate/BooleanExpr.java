package intermediate;

public class BooleanExpr extends AtomExpression {
    boolean value = false;

    public BooleanExpr(boolean value) {
        this.value = value;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return ((value) ? "#t" : "#f");
    }
}
