package intermediate;

/**
 * Represent the simple data type Symbol in Scheme. It's case-insensitive.
 */
public class Symbol extends AtomExpression {
    String name;

    public Symbol(String name) {
        this.name = name;
    }

    @Override
    public Object getValue() {
        return name;
    }

    @Override
    public String toString() {
        return "'" + name;
    }
}
