package frontend;

import java.util.regex.Pattern;

public class Token {
    public String text;
    public TokenType type;

    public Token(String text) {
        this.text = text;
        type = updateTokenType(text);
    }

    public Token(String text, TokenType type) {
        this.text = text;
        this.type = type;
    }

    protected static Pattern leftBracketPattern = Pattern.compile("^[\\{\\[\\(]&");
    protected static Pattern rightBracketPattern = Pattern.compile("^[\\}\\]\\)]&");
    protected static Pattern quotationPattern = Pattern.compile("^['`,@]$");
    protected static Pattern integerPattern = Pattern.compile("^[+-]?(\\d+)$");
    protected static Pattern doublePattern = Pattern.compile("^[+-]?((\\.\\d+)|\\d+\\.\\d+)$");

    protected static Pattern stringPattern = Pattern.compile("^\".*\"$");
    protected static Pattern booleanPattern = Pattern.compile("^#[t|f]$", Pattern.CASE_INSENSITIVE);
    protected static Pattern characterPattern = Pattern.compile("^#\\\\\\w$");

    public static TokenType updateTokenType(String tokenText) {
        TokenType type;
        if (leftBracketPattern.matcher(tokenText).find()) {
            type = TokenType.LeftBracket;
        } else if (rightBracketPattern.matcher(tokenText).find()) {
            type = TokenType.RightBracket;
        } else if (quotationPattern.matcher(tokenText).find()) {
            type = TokenType.Quotation;
        } else if (stringPattern.matcher(tokenText).find()) {
            type = TokenType.String;
        } else if (booleanPattern.matcher(tokenText).find()) {
            type = TokenType.Boolean;
        } else if (characterPattern.matcher(tokenText).find()) {
            type = TokenType.Character;
        } else if (integerPattern.matcher(tokenText).find()) {
            type = TokenType.Integer;
        } else if (doublePattern.matcher(tokenText).find()) {
            type = TokenType.Double;
        } else {
            type = TokenType.Keyword;
        }
        return type;
    }

    @Override
    public String toString() {
        return text + " [" + type + "]";
    }
}
