package frontend;

import backend.Printer;
import intermediate.*;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class Parser {
    static Pattern leftBracketPattern = Pattern.compile("\\{|\\[|\\(");
    static Pattern rightBracketPattern = Pattern.compile("\\}|\\]|\\)");
    static Pattern stringDividerPattern = Pattern.compile("\"");
    static Pattern spacePattern = Pattern.compile("\\s");
    static Pattern quotationPattern = Pattern.compile("^['`,@]$");
    static Pattern lineCommentPattern = Pattern.compile(";");

    ArrayList<Token> tokens = new ArrayList<>();
    Expression rootExpression;
    TreeMap<String, Object> symbolTable = new TreeMap<>();
    Expression currentExpression = null;
    TokenType previousType;
    int currentDepth = 0;

    /**
     * Parse the given text.
     *
     * @param text The text to be parsed
     * @throws IOException If underlying Reader pukes exception
     */
    public void parse(String text) throws IOException {
        if (text == null) {
            throw new IllegalArgumentException("text cannot be null");
        }

        StringReader stringReader = new StringReader(text);
        parse(stringReader);
        stringReader.close();
    }

    /**
     * Parse the given text.
     *
     * @param reader The input reader for reading the characters
     * @throws IOException If underlying Reader pukes exception
     */
    // todo: support multi-line comment #| ... |#
    public void parse(Reader reader) throws IOException {
        if (reader == null) {
            throw new IllegalArgumentException("reader cannot be null");
        }

        int numLines = 1;
        int charIndex = -1;

        int charCode;
        StringBuilder buffer = new StringBuilder();
        boolean isInItem = false;
        boolean isInString = false;
        boolean isInLineComment = false;

        onStart();

        while ((charCode = reader.read()) >= 0) {
            ++charIndex;
            char inputChar = (char) charCode;

            boolean isLineBreak = inputChar == '\n';
            if (isLineBreak) {
                ++numLines;
                if (isInLineComment) {
                    isInLineComment = false;
                }
                continue;
            }

            if (isInLineComment) {
                continue;
            }

            String inputCharAsString = String.valueOf(inputChar);
            boolean isStringDivider = stringDividerPattern.matcher(inputCharAsString).find();
            if (isStringDivider) {
                if (isInString) {
                    // End of the string
                    buffer.append('\"');
                    onTokenDetected(new TokenDetectedEventArgs(new Token(buffer.toString(), TokenType.String)));
                    buffer.setLength(0);
                    isInString = false;
                    isInItem = false;
                } else {
                    // Start of a string
                    buffer.append('\"');
                    isInString = true;
                    isInItem = true;
                }
                continue;
            }

            if (isInString) {
                buffer.append(inputChar);
                continue;
            }

            // Quotation
            boolean isQuotation = quotationPattern.matcher(inputCharAsString).find();
            if (isQuotation && !isInItem) {
                onTokenDetected(new TokenDetectedEventArgs(new Token(inputCharAsString, TokenType.Quotation)));
                continue;
            }

            // Bracket
            boolean isLeftBracket = leftBracketPattern.matcher(inputCharAsString).find();
            boolean isRightBracket = rightBracketPattern.matcher(inputCharAsString).find();
            boolean isBracket = isLeftBracket || isRightBracket;
            if (isBracket) {
                if (isInItem) {
                    onTokenDetected(new TokenDetectedEventArgs(new Token(buffer.toString())));
                    buffer.setLength(0);
                    isInItem = false;
                }
                if (isLeftBracket) {
                    onTokenDetected(new TokenDetectedEventArgs(new Token(inputCharAsString, TokenType.LeftBracket)));
                } else {
                    onTokenDetected(new TokenDetectedEventArgs(new Token(inputCharAsString, TokenType.RightBracket)));
                }
                continue;
            }

            boolean isSpace = spacePattern.matcher(inputCharAsString).find();
            if (isSpace) {
                if (isInItem) {
                    // End of a token
                    onTokenDetected(new TokenDetectedEventArgs(new Token(buffer.toString())));
                    buffer.setLength(0);

                    isInItem = false;
                }
            } else {
                if (isInItem) {
                    buffer.append(inputChar);
                } else {
                    if (lineCommentPattern.matcher(inputCharAsString).find()) {
                        isInLineComment = true;
                    } else {
                        // Start of a token
                        buffer.append(inputChar);
                        isInItem = true;
                    }
                }
            }
        }

        if (isInItem) {
            onTokenDetected(new TokenDetectedEventArgs(new Token(buffer.toString())));
            buffer.setLength(0);
        }

        onFinished(new FinishEventArgs(charIndex + 1, (charIndex >= 0) ? numLines : 0));
    }

    protected void onStart() {
        tokens.clear();
        rootExpression = null;
        currentExpression = null;
    }

    Token bufferedToken;
    int bufferedDepth = Integer.MIN_VALUE;

    protected void onTokenDetected(TokenDetectedEventArgs args) {
        Token token = args.token;
        tokens.add(token);
        Expression newExpression = null;

        switch (token.type) {
            case None:
                throw new ParseException(String.format("Encounter unknown token: [%s] %s", token.getClass(), token.toString()));
            case Integer:
                newExpression = new IntegerExpr(Integer.parseInt(token.text));
                break;
            case Double:
                newExpression = new DoubleExpr(Double.parseDouble(token.text));
                break;
            case String:
                newExpression = new StringExpr(token.text);
                break;
            case Boolean:
                newExpression = new BooleanExpr((token.text.equalsIgnoreCase("#t")));
                break;
            case Character:
                newExpression = new CharacterExpr(token.text.charAt(0));
                break;
        }
        switch (token.type) {
            case Integer:
            case Double:
            case String:
            case Boolean:
            case Character:
                addExpressionToCurrent(newExpression);
                break;
        }

        switch (token.type) {
            case Keyword:
                if (bufferedToken == null) {
                    newExpression = new ObjectExpr(token.text);
                } else {
                    newExpression = new Symbol(token.text);
                    symbolTable.put(token.text, null);
                    bufferedToken = null;
                    bufferedDepth = Integer.MIN_VALUE;
                }

                addExpressionToCurrent(newExpression);

                break;
            case Quotation:
                if (bufferedToken == null) {
                    bufferedToken = token;
                    bufferedDepth = currentDepth + 1;
                } else {
                    newExpression = new Expression();
                    newExpression.add(new ObjectExpr(bufferedToken.text));

                    addExpressionAndReplaceCurrent(newExpression);
                }

                break;
            case LeftBracket:
                newExpression = new Expression();
                addExpressionAndReplaceCurrent(newExpression);
                break;
            case RightBracket:
                if (currentExpression == null) {
                    throw new ParseException("Unexpected closing bracket");
                } else {
                    goUpperLevel();
                }

                break;
        }

        previousType = token.type;
    }

    protected void checkBufferedToken() {
        if (bufferedToken != null) {
            bufferedToken = null;
            bufferedDepth = currentDepth;
            Expression expr = new Expression();
            expr.add(new ObjectExpr("quote"));
            addExpressionAndReplaceCurrent(expr);
        }
    }

    protected void addExpressionAndReplaceCurrent(Expression expression) {
        checkBufferedToken();

        if (currentExpression == null) {
            rootExpression = currentExpression = expression;
        } else {
            currentExpression.add(expression);
            currentExpression = expression;
            currentDepth++;
        }
    }

    protected void goUpperLevel() {
        currentExpression = currentExpression.parent;
        currentDepth--;

        if (currentExpression == null) {
            onTreeCompleted();
            return;
        }

        if (bufferedDepth == currentDepth) {
            bufferedDepth = Integer.MIN_VALUE;
            goUpperLevel();
        }
    }

    protected void addExpressionToCurrent(Expression expression) {
        checkBufferedToken();

        if (currentExpression == null) {
            rootExpression = currentExpression = expression;
            onTreeCompleted();
        } else {
            currentExpression.add(expression);
        }
    }

    protected void onTreeCompleted() {
        System.out.println("Tokens:");
        for (Token token : tokens) {
            System.out.printf("[%s] %s\n", token.type, token.text);
        }
        tokens.clear();

        System.out.println("Expression tree:");
        Printer.print(rootExpression);

        System.out.println("Symbols:");
        symbolTable.keySet().forEach(System.out::println);
        symbolTable.clear();

        System.out.println();
    }

    protected void onFinished(FinishEventArgs args) {
        System.out.println();
    }

    public ArrayList<Token> getTokens() {
        return tokens;
    }

    public Expression getRootExpression() {
        return rootExpression;
    }

    public TreeMap<String, Object> getSymbolTable() {
        return symbolTable;
    }
}
