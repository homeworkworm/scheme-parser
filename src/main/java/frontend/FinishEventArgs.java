package frontend;

public class FinishEventArgs {
    int numChars = 0;
    int numLines = 0;

    public FinishEventArgs(int numChars, int numLines) {
        this.numChars = numChars;
        this.numLines = numLines;
    }
}
