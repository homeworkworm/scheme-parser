package frontend;

public enum TokenType {
    None,
//    Constant, // todo: replace this with Integer and Double.
    Integer,
    Double,
    Keyword,
    Quotation,
    LeftBracket,
    RightBracket,
    String,
    Boolean,
    Character,
    Symbol,
}
