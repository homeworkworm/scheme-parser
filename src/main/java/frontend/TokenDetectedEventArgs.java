package frontend;

public class TokenDetectedEventArgs {
    public Token token;

    public TokenDetectedEventArgs(Token token) {
        this.token = token;
    }
}
