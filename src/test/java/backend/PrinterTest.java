package backend;

import frontend.Parser;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PrinterTest {
    Parser parser = new Parser();


    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testPrint1() throws Exception {
        parser.parse("(list 1 2 (car (3 4)))");
        Printer.print(parser.getRootExpression());
    }

    @Test
    public void testPrint2() throws Exception {
        parser.parse("(define (x) 18)");
        Printer.print(parser.getRootExpression());
    }

    @Test
    public void testRealProgram1() throws Exception {
        parser.parse("(test (1) (2) (3))");
        Printer.print(parser.getRootExpression());
    }

    @Test
    public void testRealProgram2() throws Exception {
        parser.parse("(test ('xy) (2))");
        Printer.print(parser.getRootExpression());
    }

    @Test
    public void testRealProgram3() throws Exception {
        parser.parse("(cond ((null? term ) '()) ((not (member? '^ term)) )");
        Printer.print(parser.getRootExpression());
    }

    @Test
    public void testRealProgram4() throws Exception {
        parser.parse("(cond ((null? term ) '()) ((not (member? var term)) '(0)) ((not (member? '^ term)) (upto var term)) (else (deriv-term-expo term var)) )");
        Printer.print(parser.getRootExpression());
    }

    @Test
    public void testRealProgram5() throws Exception {
        parser.parse("(cond ((null? poly) '()) (else (cons (upto '+ poly) (terminize (after '+ poly) ) ) ) )");
        Printer.print(parser.getRootExpression());
    }

    @Test
    public void testRealProgram6() throws Exception {
        parser.parse("(define deriv\n" +
                "  (lambda (poly var)\n" +
                "    (let* ((terms (terminize poly)) ; \"terminize\" the polynomial\n" +
                "           (deriv-term              ; local procedure deriv-term \n" +
                "             (lambda (term)\n" +
                "               (cond\n" +
                "                 ((null? term) '())\n" +
                "                 ((not (member? var term)) '(0))           ; deriv = 0\n" +
                "                 ((not (member? '^ term)) (upto var term)) ; deriv = coeff\n" +
                "                 (else (deriv-term-expo term var))         ; handle exponent\n" +
                "             )))\n" +
                "           (diff (map deriv-term terms)))   ; map deriv-term over the terms\n" +
                "      (remove-trailing-plus (polyize diff)) ; finalize the answer\n" +
                ")))");
        Printer.print(parser.getRootExpression());
    }

    @Test
    public void testRealProgram7() throws Exception {
        parser.parse("(define terminize\n" +
                "  (lambda (poly)\n" +
                "    (cond\n" +
                "      ((null? poly) '())\n" +
                "      (else (cons (upto '+ poly) (terminize (after '+ poly))))\n" +
                ")))");
        Printer.print(parser.getRootExpression());
    }
}
