package frontend;

import backend.Printer;
import intermediate.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.TreeMap;

import static org.testng.Assert.assertEquals;

public class ParserTest {
    Parser parser = new Parser();

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testNormal() throws Exception {
        parser.parse("(+ 1 2)");
        assertEquals(parser.getTokens().size(), 5);

        Expression expression = parser.getRootExpression();
        assertEquals(expression.expressions.size(), 3);

        Expression exp0 = expression.expressions.get(0);
        assertEquals(exp0.getClass(), ObjectExpr.class);
        assertEquals(exp0.toString(), "+");
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void testNull() throws Exception {
        parser.parse((String) null);
        Assert.fail();
    }

    @Test
    public void testEmpty() throws Exception {
        parser.parse("");
        assertEquals(parser.getTokens().size(), 0);
        Assert.assertNull(parser.getRootExpression());
    }

    @Test
    public void testEmptyInputText() throws Exception {
        parser.parse("   ");
        assertEquals(parser.getTokens().size(), 0);
        Assert.assertNull(parser.getRootExpression());
    }

    // Comment
    @Test
    public void testLineComment() throws Exception {
        parser.parse("; This is a comment  \n(print \"Hi\") ");
        assertEquals(parser.getTokens().size(), 4);
    }

    @Test
    public void testLineCommentInTheMiddle() throws Exception {
        parser.parse("(define ; x( 28) \n x 18)");
        assertEquals(parser.getTokens().size(), 5);
    }

    @Test
    public void testBlockComment() throws Exception {
        parser.parse("#| >< |# (print 123)");
        assertEquals(parser.getTokens().size(), 4);
    }

    @Test
    public void testBlockCommentInTheMiddle() throws Exception {
        parser.parse("(print #| >< |# 123)");
        assertEquals(parser.getTokens().size(), 4);
    }

    // Keyword
    @Test(groups = {"keyword"})
    public void testKeyword() throws Exception {
        parser.parse("(print 'xyz)");
        assertEquals(parser.getTokens().size(), 5);
    }

    @Test(groups = {"keyword"})
    public void testKeywordWithQuote() throws Exception {
        parser.parse("(print 'xyz')");
        assertEquals(parser.getTokens().size(), 5);
    }

    // Quotation
    @Test(groups = {"quotation"})
    public void testQuotationWithString() throws Exception {
        parser.parse("(print '\"test\")");
        assertEquals(parser.getTokens().size(), 5);
    }

    @Test(groups = {"quotation"})
    public void testQuotationWithNumber() throws Exception {
        parser.parse("(print '12)");
        Expression root = parser.getRootExpression();
        Expression expr1 = root.expressions.get(1);
        assertEquals(expr1.getClass(), Expression.class);
        Expression expr1_1 = expr1.expressions.get(1);
        assertEquals(expr1_1.getClass(), IntegerExpr.class);
        assertEquals(expr1_1.getValue(), 12);
    }

    @Test(groups = {"quotation"})
    public void testQuotationWithQuotation() throws Exception {
        parser.parse("(print '`a)");
        assertEquals(parser.getTokens().size(), 6);
    }

    @Test(groups = {"quotation"})
    public void testComplexQuotation1() throws Exception {
        parser.parse("(print `\"`abc')d\")");   // (print `"`asd')f")
        assertEquals(parser.getTokens().size(), 5);
    }

    @Test(groups = {"quotation"})
    public void testComplexQuotation2() throws Exception {
        parser.parse("(print '((1 \"string\") 3))");   // (print '((1 "string") 3))
        assertEquals(parser.getTokens().size(), 11);
    }

    // Number
    @Test(groups = {"number"})
    public void testSingleInteger() throws Exception {
        parser.parse("123");
        assertEquals(parser.getRootExpression().getValue(), 123);
    }

    @Test(groups = {"number"})
    public void testNegative() throws Exception {
        parser.parse("-123");
        assertEquals(parser.getRootExpression().getValue(), -123);
    }

    @Test(groups = {"number"})
    public void testFloat1() throws Exception {
        parser.parse("-1.23");
        assertEquals(parser.getRootExpression().getValue(), -1.23);
    }

    @Test(groups = {"number"})
    public void testFloat2() throws Exception {
        parser.parse("-.23");
        assertEquals(parser.getRootExpression().getValue(), -.23);
    }

    // Bracket
    @Test
    public void testWeirdBracket() throws Exception {
        parser.parse("(car [ #f 2 3)}");
        assertEquals(parser.getTokens().size(), 8);
    }

    // Character
    @Test
    public void testCharacter() throws Exception {
        parser.parse("#\\c");
        assertEquals(parser.getTokens().size(), 1);
        assertEquals(parser.getRootExpression().expressions.size(), 0);
        assertEquals(parser.getRootExpression().getClass(), CharacterExpr.class);
    }

    // Pair
    @Test
    public void testOnlyPair() throws Exception {
        parser.parse("(2 . 1)");
        assertEquals(parser.getTokens().size(), 5);
    }

    @Test
    public void testPair() throws Exception {
        parser.parse("(print (2 . 1)");
        assertEquals(parser.getTokens().size(), 7);
    }

    // List
    @Test
    public void testEmptyList() throws Exception {
        parser.parse("(print `())");
        assertEquals(parser.getTokens().size(), 6);
    }

    @Test
    public void testListWithSingleItem() throws Exception {
        parser.parse("(print '(12)");
    }

    @Test
    public void testListWithMultipleItems() throws Exception {
        parser.parse("(print '(1 2)");
    }

    @Test
    public void testListWithNestedExpression1() throws Exception {
        parser.parse("(list 1 2 (car (3 4)))");
        assertEquals(parser.getTokens().size(), 12);

        parser.parse("(print '(1 2 (car (3 4))))");
        assertEquals(parser.getTokens().size(), 15);
    }

    @Test
    public void testListWithNestedExpression2() throws Exception {
        parser.parse("((null? term ) '2)");
        Expression root = parser.getRootExpression();
        assertEquals(root.expressions.size(), 2);

        Expression expr1 = root.expressions.get(0);
        assertEquals(expr1.expressions.size(), 2);
        assertEquals(expr1.getClass(), Expression.class);

        Expression expr2 = root.expressions.get(1);
        assertEquals(expr2.expressions.size(), 2);
        assertEquals(expr2.getClass(), Expression.class);
    }

    // Symbol
    @Test
    public void testSimpleProcedureWithSymbol() throws Exception {
        TreeMap<String, Object> symbolTable;

        parser.parse("(print 'hello)");
        Expression expression = parser.getRootExpression();
        assertEquals(expression.expressions.size(), 2);
        Expression expr2 = expression.expressions.get(1);
        assertEquals(expr2.getClass(), Symbol.class);
        assertEquals(expr2.getValue(), "hello");
    }
}
